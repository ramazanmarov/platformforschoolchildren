package com.example.network.api

import com.example.network.models.LoginQuery
import com.example.network.models.LoginResponse
import com.example.network.models.RegisterQuery
import com.example.network.models.RegisterResponse
import com.example.network.models.RepassResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.*


interface AppApi {

    //Verification

    @POST("/auth")
    fun auth(@Body q: LoginQuery): Observable<LoginResponse>

    @POST("/register")
    fun register(@Body q: RegisterQuery): Observable<RegisterResponse>


    @POST("/reset/{resetToken}")
    fun resetToken(
        @Path("resetToken") resetToken: String,
        @Query("password") password: String,
    )

    @GET("/reset")
    fun resetPassword(
        @Query("email") email: String): Observable<RepassResponse>

    //Paid Courses

    @POST("/paidcourses/enroll/{courseId}")
    fun enrollInCourse(@Path("courseId") courseId: Int)

    @GET("/paidcourses")
    fun getPaidCourses()

    //Quiz

    @GET("/quiz/{testId}")
    fun getTestFromId(@Path("testId") testId: Int)

    @GET("/quiz/{testId}/{currentQuestionIndex}/prev")
    fun getCurrentQuestionIndex(
        @Path("testId") testId: Int,
        @Path("currentQuestionIndex") currentQuestionIndex: Int,
    )

    @GET("/quiz/{testId}/{currentQuestionIndex}/next")
    fun getNextQuestion(
        @Path("testId") testId: Int,
        @Path("currentQuestionIndex") currentQuestionIndex: Int,
    )

    @GET("/quiz/res/{quizId}")
    fun getTextAnswers(
        @Path("quizId") quizId: Int,
        @Query("answersIds") answersIds: Array<Int>,
    )

    //FCourses


    //User

    @PUT("/user/user/update/")
    fun updateUserInfo()

    @PUT("/user/sub/{courseId}")
    fun getToFav(@Path("courseId") courseId: Int)

    @PUT("/user/feedback")
    fun feedback()

    @GET("/user/personal-info")
    fun getUserInfo()

    @GET("/user/course/get/{id}")
    fun getUserCourse(@Path("id") id: Int)

    @GET("/user/course/get/all")
    fun getAllUserCourse()

    //Comment

    @PUT("/comment/update/{courseId}/{commentId}")
    fun updateComment()

    @POST("/comment/create/{courseId}")
    fun createComment(@Path("courseId") courseId: Int)

    @GET("/comment/get/{commentId}")
    fun getComment(@Path("commentId") commentId: Int)

    @GET("/comment/get/user")
    fun getUserComment()

    @GET("/comment/get/course/{courseId}")
    fun getCourseComment(@Path("courseId") courseId: Int)

    @GET("/comment/get/all")
    fun getAllComment()

    @DELETE("/comment/delete/{commentId}")
    fun deleteComment(@Path("commentId") commentId: Int)
}