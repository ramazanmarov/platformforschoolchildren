package com.example.network.models

data class LoginQuery (
    var email: String,
    var password: String
)

data class LoginResponse(
    var auth_token: String,
    val ref_token: String
)

data class RepassQuery(
    val email: String
)

data class RepassResponse(
    val message: String
)
