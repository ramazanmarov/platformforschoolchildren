package com.example.network.models

import java.io.Serializable

data class Favorite(
    var message: String
)

data class FCourse(
    var id: Int,
    var name: String,
    var duraction: String,
    var size: Int,
    var isFav: Boolean = false
    ): Serializable

data class PCourse(
    var id: Int,
    var name: String,
    var duraction: String,
    var isFav: Boolean = false,
    var size: Int,
    var price: Int,
    var startDate: String
): Serializable

data class Category(
    var id: Int,
    var name: String
):Serializable

enum class Status {
    TODO,
    IN_PROCESS,
    IN_REVIEW,
    DONE
}