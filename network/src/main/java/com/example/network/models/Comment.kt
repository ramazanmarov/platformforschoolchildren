package com.example.network.models

import java.io.Serializable

data class Comment(
    val email: String,
    val comment: String
):Serializable
