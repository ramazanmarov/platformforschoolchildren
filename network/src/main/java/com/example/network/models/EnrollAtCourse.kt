package com.example.network.models

import java.io.Serializable

data class EnrollAtCourseRequest(
    val id: Int,
    val fullName: String,
    val phoneNumber: String,
    val courseName: String,
    val dateOfCreation: String
):Serializable
