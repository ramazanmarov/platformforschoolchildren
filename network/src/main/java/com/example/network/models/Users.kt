package com.example.network.models

import java.io.Serializable


data class RegisterQuery(
   var email: String,
   var password: String,
   val name: String,
   val surname: String,
   var schoolNumber: Int,
   var schoolName: String,
   var schoolGrade: String,
   var schoolLocation: String
):Serializable

data class RegisterResponse(
    var id: String
):Serializable





