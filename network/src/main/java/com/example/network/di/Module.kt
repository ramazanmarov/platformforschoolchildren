package com.example.network.di

import com.example.network.base.Schedulers
import org.koin.dsl.module

val networkModule = module {
    single { provideOkHttp(get()) }
    single { provideRetrofit(get(),   "http://35.246.189.147:8082") }
    single { Schedulers() }
    factory { provideSession(get()) }
}