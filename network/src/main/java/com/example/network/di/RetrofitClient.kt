package com.example.network.di

import com.example.core.storage.Preferences
import com.google.gson.GsonBuilder
import com.example.network.api.AppApi
import com.example.network.base.ResponseConvertorFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

//class RetrofitClient {
//    companion object {
//        private const val baseUrl = "http://192.168.43.19:8082"
//        var appContext: Context? = null
//
//        private val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
//            this.level = HttpLoggingInterceptor.Level.BODY
//        }

fun provideOkHttp(preferences: Preferences): OkHttpClient {
    val builder = OkHttpClient.Builder()
    if (com.example.network.BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        builder.readTimeout(60, TimeUnit.SECONDS)
        builder.writeTimeout(60, TimeUnit.SECONDS)
        builder.addInterceptor(loggingInterceptor)
    }
    builder.addInterceptor(getHeaderInterceptor(preferences))
    return builder.build()
}

private fun getHeaderInterceptor(preferences: Preferences): Interceptor {
    return object : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            if (preferences.token != null) {
                return chain.proceed(
                    chain
                        .request()
                        .newBuilder()
                        .header("Authorization", preferences.token!!)
                        .build()
                )
            } else {
                return chain.proceed(
                    chain
                        .request()
                        .newBuilder()
                        .build()
                )
            }
        }
    }
}

fun provideRetrofit(okHttpClient: OkHttpClient, url: String): Retrofit {
    return Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(url)
        .addConverterFactory(GsonConverterFactory.create())
        .addConverterFactory(ScalarsConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(NullConverterFactory())
        .addConverterFactory(ResponseConvertorFactory(GsonBuilder().enableComplexMapKeySerialization()
            .create()))
        .build()
}

fun provideSession(retrofit: Retrofit) = retrofit.create(AppApi::class.java)

//        private val client: OkHttpClient = OkHttpClient.Builder().apply {
//            this.addInterceptor(interceptor)
//            this.addInterceptor(getHeaderInterceptor(Preferences(App.context())))
//        }.build()
//
//        var gson = GsonBuilder()
//            .setLenient()
//            .create()

//        fun getRetrofitInstance(): Retrofit {
//            return Retrofit.Builder()
//                .baseUrl(baseUrl)
//                .client(client)
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .build()
//        }
//    }
//}