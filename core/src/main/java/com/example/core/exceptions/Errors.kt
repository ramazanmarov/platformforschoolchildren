package com.example.core.exceptions

class ServerError(override val message: String?): Exception(message)
class NoDigitalSignatureException(override val message: String): Exception(message)