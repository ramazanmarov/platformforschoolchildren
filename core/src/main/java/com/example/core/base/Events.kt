package com.example.core.base

sealed class Event{
    class Success: Event()
    class Fail(val message: String): Event()
    class Message(val message: String): Event()
}

sealed class SplashEvents: Event(){
    class OpenMain: SplashEvents()
    class ShowCompleteRegister: SplashEvents()
    class ShowSignInEvent: SplashEvents()
    class GetDeviceId: SplashEvents()

}

sealed class PhoneRegistrationEvents : Event() {
    class SuccessSmsSend(val sessionId: String) : PhoneRegistrationEvents()
}

sealed class SignEvents : Event() {
    class ShowUserName(val name: String) : SignEvents()
    class OpenEmployeeInn(val orgInn: String) : SignEvents()
    class RegistrationRequest(val message: String) : SignEvents()
}

sealed class MainEvents : Event() {
    class SuccessUserUpdated(): MainEvents()
    class FailureUserUpdated(): MainEvents()
}

sealed class ShiftEvents : Event() {
    class NoOpenedShift(): ShiftEvents()
    class OpenedShift(var shiftNumber: Int): ShiftEvents()

}

sealed class ProfileEvents : Event() {
    class SuccessUpdateUserEvent : ProfileEvents()
    class NeedNewSigninEvent : ProfileEvents()
}

sealed class RegistrationCompleteEvents : Event() {
    class SuccessComplete : RegistrationCompleteEvents()
    class RegistrationNotFound : RegistrationCompleteEvents()
    class RegistrationRefused(val moderatedResult: Any) : RegistrationCompleteEvents()
    class WaitingComplete : RegistrationCompleteEvents()
}

sealed class ModerationEvents : Event() {
    class CancelRegistration : ModerationEvents()
}


