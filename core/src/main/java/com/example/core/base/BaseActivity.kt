package com.example.core.base

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.viewbinding.ViewBinding
import com.example.core.custom.FullScreenDialog
import com.example.core.until.LocaleHelper
import kotlinx.coroutines.cancelChildren
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<VM : ViewModel> : AppCompatActivity() {

    lateinit var viewModel: VM
    protected abstract fun getViewModelClass(): Class<VM>

    private val exceptionHandler: ServerErrorHandler by inject { parametersOf(this) }
    private var alertDialog: FullScreenDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this)[getViewModelClass()]
        subscribeToViewModelLiveData()
    }

    override fun onDestroy() {
        (viewModel as? CoreViewModel<*>)?.disposable?.dispose()
        super.onDestroy()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase!!))
    }

    private fun subscribeToViewModelLiveData() {
        (viewModel as? CoreViewModel<*>)?.let { it ->
            it.showProgress.observe(this, Observer {
                when (it) {
                    true -> showProgressDialog()
                    false -> hideProgressDialog()
                }
            })
            it.exceptionHandler = exceptionHandler
        }
    }

    fun showProgressDialog() {
        if (alertDialog != null) {
            alertDialog?.dismiss()
            alertDialog = null
        }

        alertDialog = FullScreenDialog()
        alertDialog?.setOnCancelListener {
            (viewModel as? CoreViewModel<*>)?.viewModelScope?.coroutineContext?.cancelChildren()
        }
        alertDialog?.show(supportFragmentManager, "")
    }

    fun hideProgressDialog() {
        alertDialog?.dismiss()
        alertDialog = null
    }
}

