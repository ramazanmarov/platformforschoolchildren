package com.example.core.base

import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.example.core.exceptions.ServerError
import com.example.core.storage.Preferences
import org.json.JSONObject
import retrofit2.HttpException
import java.lang.NullPointerException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.CancellationException
import java.util.concurrent.TimeoutException

class ServerErrorHandler (val context: Context){
    private fun noInternetConnection(){
        Toast.makeText(context, "Отсутствует подключение к интернету, проверьте подключение", Toast.LENGTH_LONG)
            .show()
    }

    private fun handleTimeout(){
        Toast.makeText(context, "Сервер временно недоступен, повторите попытку немного п...", Toast.LENGTH_LONG)
            .show()
    }

    private fun handleTokenExpired(){
        Preferences(context).logOut()
        context.startActivity(Intent(
            context,
            Class.forName("platformforschoolchildren.ui.splash.SplashActivity")
        ))
        (context as? BaseActivity<*>)?.finishAffinity()
    }

    private fun handleCommonException(message: String? = null){
        if (message.isNullOrEmpty()){
            handleUnknownError(); return
        }
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    private fun handleUnknownError(){
        Toast.makeText(context, "Произошла неизвестная ошибка, повторите запрос позднее", Toast.LENGTH_LONG)
            .show()
    }

    private fun handleHttpException(e: HttpException){
        try {
            val code = e.code()
            val message = e.response()?.errorBody()?.string()
            when(code){
                401 -> handleTokenExpired()
                204 -> "Забронировано!"
                else -> handleCommonException(parseMessage(message))
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }

    private fun parseMessage(message: String?): String?{
        return if (message.isNullOrEmpty()) null
        else {
            try {
                JSONObject(message).getString("message")
            }catch (e: Exception){
                message
            }
        }
    }

    fun handle(e: Throwable){
        when(e){
            is CancellationException -> {}
            is HttpException -> handleHttpException(e)
            is UnknownHostException, is ConnectException -> noInternetConnection()
            is TimeoutException, is SocketTimeoutException -> handleTimeout()
            is ServerError -> handleCommonException(e.message)
            is NullPointerException -> "Забронировано!"
            else -> handleCommonException()
        }
    }
}