package com.example.core.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch

open class CoreViewModel<Event>: ViewModel() {
    var event = MutableLiveData<Event>()
    var showProgress = MutableLiveData<Boolean>()

    var disposable: CompositeDisposable = CompositeDisposable()
    lateinit var exceptionHandler: ServerErrorHandler

    fun showProgress() = showProgressState(true)
    fun hideProgress() = showProgressState(false)

    private fun showProgressState(isShow: Boolean){
        showProgress.value = isShow
    }

    fun runWithProgress(block: suspend () -> Unit){
        showProgress()
        viewModelScope.launch {
            try {
                block.invoke()
            }catch (e: Exception){
                exceptionHandler.handle(e)
            }finally {
                hideProgress()
            }
        }
    }

}