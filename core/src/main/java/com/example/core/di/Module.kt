package com.example.core.di

import android.content.Context
import com.example.core.base.ServerErrorHandler
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val baseModule = module {
    single { com.example.core.storage.Preferences(get()) }
    single { androidContext().resources }
    factory { (context: Context) -> ServerErrorHandler(context) }
}
