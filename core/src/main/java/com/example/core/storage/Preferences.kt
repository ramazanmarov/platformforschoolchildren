package com.example.core.storage

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class Preferences(val context: Context) {
private val sharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(context)
    }

    var token: String?
        get(){
            return getStringByKey(PrefKeys.TOKEN)
        }
        set(value) {
            setStringByKey(PrefKeys.TOKEN, value!!)
        }

    var executorFlag: Boolean
    get() {
        return getBooleanByKey(PrefKeys.FIRST_LAUNCH)
    }
    set(value){
        setBooleanByKey(PrefKeys.FIRST_LAUNCH, value)
    }

    private fun getBooleanByKey(key: String): Boolean = sharedPreferences.getBoolean(key, false)
    private fun setBooleanByKey(key: String, value: Boolean){
        sharedPreferences
            .edit()
            .putBoolean(key, value)
            .apply ()
    }

    private fun setStringByKey(token: String, value: String) {
        sharedPreferences
            .edit()
            .putString(token, value)
            .apply()
    }

    private fun getStringByKey(key: String): String? =
        sharedPreferences.getString(key, "")

    var authorName: String?
        get(){
            return getStringByName(PrefKeys.AUTHOR_NAME)
        }
        set(value) {
            setStringByName(PrefKeys.AUTHOR_NAME, value!!)
        }

    private fun setStringByName(name: String, value: String) {
        sharedPreferences
            .edit()
            .putString(name, value)
            .apply()
    }

     val removedStatus = "Removed"
     val addedStatus = "Added"

    private fun getStringByName(name: String): String? =
        sharedPreferences.getString(name, "")

    var userId: String?
        get(){
            return getStringById(PrefKeys.USER_ID)
        }
        set(value) {
            setStringById(PrefKeys.USER_ID, value!!)
        }

    private fun setStringById(name: String, value: String) {
        sharedPreferences
            .edit()
            .putString(name, value)
            .apply()
    }

    private fun getStringById(name: String): String? =
        sharedPreferences.getString(name, "")


    var authorId: String?
        get(){
            return getStringByAuthorId(PrefKeys.AUTHOR_ID)
        }
        set(value) {
            setStringByAuthorId(PrefKeys.AUTHOR_ID, value!!)
        }

    private fun setStringByAuthorId(name: String, value: String) {
        sharedPreferences
            .edit()
            .putString(name, value)
            .apply()
    }

    private fun getStringByAuthorId(name: String): String? =
        sharedPreferences.getString(name, "")

    fun logOut(){
         token = ""
        authorId = ""
        userId = ""
        authorName = ""
        executorFlag = false
    }

    object PrefKeys{
        const val AUTHOR_NAME = ""
        const val USER_ID = ""
        const val AUTHOR_ID = ""
        const val TOKEN = "TOKEN"
        const val FIRST_LAUNCH = "FIRST_LAUNCH"
    }


}