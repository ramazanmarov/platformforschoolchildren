package com.example.core.custom

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.core.R
import com.google.gson.annotations.Until

class FullScreenDialog: DialogFragment() {

    private var onCancel: (()-> Until)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AlertDialog_FullScreenDialogStyle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        super.onCreateView(inflater, container, savedInstanceState)
       val view = inflater.inflate(R.layout.fullscreen_progress, container, false)
//        view.progress.start()
        return view
    }

    override fun onStart() {
        super.onStart()
        dialog?.apply {
            onCancel?.let { setOnCancelListener { it() } }
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            window?.setLayout(width, height)
        }
    }

    fun setOnCancelListener(onCancel: () -> Unit){
//        this.onCancel = onCancel
    }
}