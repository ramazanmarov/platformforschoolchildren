package com.example.platformforschoolchildren.di

import com.example.platformforschoolchildren.network.repository.Repository
import com.example.platformforschoolchildren.ui.main.MainVM
import com.example.platformforschoolchildren.ui.profile.ProfileVM
import com.example.platformforschoolchildren.ui.splash.SplashVM
import com.example.platformforschoolchildren.ui.verification.login.LoginVM
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module{
    viewModel{ MainVM() }
    viewModel { LoginVM(get(), get()) }
    viewModel { ProfileVM() }
    viewModel { SplashVM(get()) }
    factory { Repository(get(), get()) }
}

