package com.example.platformforschoolchildren.application

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.example.core.di.baseModule
import com.example.network.di.networkModule
import com.example.core.until.LocaleHelper
import com.example.platformforschoolchildren.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidContext(this@App)
            modules(appModule, networkModule, baseModule)
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base!!))
    }
}