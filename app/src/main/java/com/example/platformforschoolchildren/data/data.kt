package com.example.platformforschoolchildren.data

import com.example.network.models.FCourse
import com.example.network.models.PCourse

object data {
    val fList = mutableListOf<FCourse>(
        FCourse(1, "Русский язык", "3 месяца", 30, false),
        FCourse(2, "Английский язык", "3 месяца", 30, false),
        FCourse(3, "Химия", "3 месяца", 30, false),
        FCourse(4, "Биология", "3 месяца", 30, false),
        FCourse(5, "Программирование", "3 месяца", 30, false)
    )
    val pListData = mutableListOf<PCourse>(
        PCourse(1, "Python", "3 месяца", false, 40, 20000, "1 september"),
        PCourse(2, "Kotlin", "3 месяца", false, 50, 25000, "1 september"),
        PCourse(3, "Java", "3 месяца", false, 48, 19000, "1 september"),
        PCourse(4, "CSS", "3 месяца", false, 65, 30000, "1 september"),
        PCourse(5, "PHP", "3 месяца", false, 32, 29000, "1 september")
    )

    @JvmName("getFList1")
    fun getFList(): MutableList<FCourse> = fList
    fun getPList(): MutableList<PCourse> = pListData
}