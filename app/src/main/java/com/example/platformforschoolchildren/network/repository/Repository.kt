package com.example.platformforschoolchildren.network.repository

import com.example.network.api.AppApi
import com.example.network.models.RegisterQuery
import com.example.network.models.RegisterResponse
import com.example.core.storage.Preferences
import com.example.network.models.LoginQuery
import com.example.network.models.LoginResponse
import com.example.network.models.RepassQuery
import com.example.network.models.RepassResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers


class Repository (private var appApi: AppApi,
                  private val prefs: Preferences){

    fun registration(params: RegisterQuery): Observable<RegisterResponse>{
        val email = params.email
        val password = params.password
        val name = params.name
        val surname = params.surname
        val schoolNumber = params.schoolNumber
        val schoolName = params.schoolName
        val schoolGrade = params.schoolGrade
        val schoolLocation = params.schoolLocation

        return appApi.register(RegisterQuery(
            email, password, name, surname, schoolNumber, schoolName, schoolGrade, schoolLocation))
            .subscribeOn(io.reactivex.schedulers.Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun makeLogin(params: LoginQuery): Observable<LoginResponse>{
        val email = params.email
        val password = params.password

        return appApi.auth(LoginQuery(email, password))
            .subscribeOn(io.reactivex.schedulers.Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun resetPassword(param: RepassQuery): Observable<RepassResponse>{
        val email = param.email

        return appApi.resetPassword(email)
            .subscribeOn(io.reactivex.schedulers.Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }


}
