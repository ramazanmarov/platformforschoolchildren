package com.example.platformforschoolchildren.ui.verification.registration

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.example.core.base.BaseFragment
import com.example.core.base.Event
import com.example.platformforschoolchildren.R
import com.example.platformforschoolchildren.databinding.FragmentRegist2Binding
import com.example.network.models.RegisterQuery
import com.example.platformforschoolchildren.databinding.FragmentRegistBinding
import com.example.platformforschoolchildren.ui.verification.login.LoginVM

class Regist2Fragment : BaseFragment<LoginVM, FragmentRegist2Binding>() {
    override fun getViewModelClass() = LoginVM::class.java

    override fun getViewBinding() = FragmentRegist2Binding.inflate(layoutInflater)

    val args: Regist2FragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupView()
        initListeners()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subLiveData()
    }

    private fun setupView() {

    }

    private fun initListeners() {

        binding.btnDone.setOnClickListener {
            if (binding.name.text.isNullOrEmpty()
                && binding.surname.text.isNullOrEmpty()
                && binding.schoolNumb.text.isNullOrEmpty()
                && binding.schoolGrad.text.isNullOrEmpty()
                && binding.schoolLocation.text.isNullOrEmpty()
                && binding.schoolName.text.isNullOrEmpty()
            ) {
                Toast.makeText(requireContext(), "Заполните все поля!", Toast.LENGTH_SHORT).show()
            } else {
                    makeRegist()

            }
        }
    }

    fun subLiveData(){
        viewModel.event.observe(viewLifecycleOwner, Observer {
            when (it){
                is Event.Success -> {
                    val action = Regist2FragmentDirections.actionRegist2FragmentToLoginFragment()
                    view?.let { it5 -> Navigation.findNavController(it5).navigate(action) }
                }
                is Event.Fail -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }

                else -> {}
            }
        })
    }

    fun makeRegist() {
        val email = args.emailPass
        val pass = args.pass

        val regInfo = RegisterQuery(email, pass, binding.name.text.toString(),
            binding.surname.text.toString(), binding.schoolNumb.text.toString().toInt(),
            binding.schoolName.text.toString(), binding.schoolGrad.text.toString(),
            binding.schoolLocation.text.toString())

        viewModel.registration(regInfo)
    }

    companion object {
        fun newInstance() = Regist2Fragment()
    }
}