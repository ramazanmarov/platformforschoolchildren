package com.example.platformforschoolchildren.ui.main

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.media3.common.MediaItem
import androidx.media3.common.MimeTypes
import androidx.media3.datasource.DefaultDataSource
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.source.ProgressiveMediaSource
import androidx.media3.ui.PlayerView
import androidx.navigation.fragment.navArgs
import com.example.core.base.BaseFragment
import com.example.platformforschoolchildren.R
import com.example.platformforschoolchildren.databinding.CustomControllerBinding
import com.example.platformforschoolchildren.databinding.FragmentFCourseDetailsBinding
import com.example.platformforschoolchildren.ui.verification.login.LoginVM

class FCourseDetailsFragment : BaseFragment<MainVM, FragmentFCourseDetailsBinding>() {

    lateinit var playerView: PlayerView
    val args: FCourseDetailsFragmentArgs by navArgs()
    override fun getViewModelClass() = MainVM::class.java

    override fun getViewBinding() = FragmentFCourseDetailsBinding.inflate(layoutInflater)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupView()
        initListeners()
    }

    private fun setupView() {
        val arg = args
        initPlayer()

//        simpleExoPlayer = ExoPlayer.Builder(requireContext())
//            .build()
//            .apply {
//
//                val source = if (mediaUrl.contains("m3u8"))
//                    getHlsMediaSource()
//                else
//                    getProgressiveMediaSource()
//
//                setMediaSource(source)
//                prepare()
//                addListener(playerListener)
//            }
    }

    fun initPlayer() {
        playerView = binding.videoView

        val player = ExoPlayer.Builder(requireContext()).build()
        playerView.player = player

        val mediaItem = MediaItem.Builder()
            .setUri("https://samplelib.com/lib/preview/mp4/sample-5s.mp4")
            .setMimeType(MimeTypes.APPLICATION_MP4)
            .build()

        val mediaSource = ProgressiveMediaSource.Factory(
            DefaultDataSource.Factory(requireContext())
        ).createMediaSource(mediaItem)

        player.setMediaItem(mediaItem)
        player.prepare()
        player.play()
    }

//    private fun getHlsMediaSource(): MediaSource {
//        // Create a HLS media source pointing to a playlist uri.
//        return HlsMediaSource.Factory(dataSourceFactory).
//        createMediaSource(MediaItem.fromUri(mediaUrl))
//    }
//
//    private fun getProgressiveMediaSource(): MediaSource{
//        // Create a Regular media source pointing to a playlist uri.
//        return ProgressiveMediaSource.Factory(dataSourceFactory)
//            .createMediaSource(MediaItem.fromUri(Uri.parse(mediaUrl)))
//    }
//
//    private fun releasePlayer(){
//        simpleExoPlayer?.apply {
//            playWhenReady = false
//            release()
//        }
//        simpleExoPlayer = null
//    }
//
//    private fun pause(){
//        simpleExoPlayer?.playWhenReady = false
//    }

//    private fun play(){
//        simpleExoPlayer?.playWhenReady = true
//    }
//
//    private fun restartPlayer(){
//        simpleExoPlayer?.seekTo(0)
//        simpleExoPlayer?.playWhenReady = true
//    }
//
//    private val playerListener = object: Player.Listener {
//        override fun onPlaybackStateChanged(playbackState: Int) {
//            super.onPlaybackStateChanged(playbackState)
//            when(playbackState){
//                STATE_ENDED -> restartPlayer()
//                STATE_READY -> {
//                    binding.videoView.player = simpleExoPlayer
//                    play()
//                }
//            }
//        }
//    }

    private fun initListeners() {

    }

}