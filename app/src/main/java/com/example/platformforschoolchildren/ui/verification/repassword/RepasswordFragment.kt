package com.example.platformforschoolchildren.ui.verification.repassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.core.base.BaseFragment
import com.example.platformforschoolchildren.databinding.FragmentRepasswordBinding
import com.example.platformforschoolchildren.ui.verification.login.LoginVM


class RepasswordFragment : BaseFragment<LoginVM, FragmentRepasswordBinding>() {
    override fun getViewModelClass() = LoginVM::class.java

    override fun getViewBinding() = FragmentRepasswordBinding.inflate(layoutInflater)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        initListeners()
    }

    private fun setupView(){

    }

    private fun initListeners(){


    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, RepasswordFragment::class.java))
        }
    }
}