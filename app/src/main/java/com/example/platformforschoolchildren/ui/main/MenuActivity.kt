package com.example.platformforschoolchildren.ui.main

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.core.base.BaseActivity
import com.example.platformforschoolchildren.R
import com.example.platformforschoolchildren.databinding.ActivityMenuBinding
import com.example.platformforschoolchildren.ui.favorite_cources.FavoriteFragment
import com.example.platformforschoolchildren.ui.p_courses.PCoursesFragment
import com.example.platformforschoolchildren.ui.profile.ProfileFragment

class MenuActivity : AppCompatActivity(){
    lateinit var binding: ActivityMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        binding.root
        setupView()
    }

    private fun setupView(){


        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostController
        binding.btmMenu.setupWithNavController(navHostFragment)


        navHostFragment.addOnDestinationChangedListener{_, destination, _->
            when(destination.id){
                R.id.FCoursesFragment,
                R.id.PCoursesFragment,
                R.id.favoriteFragment,
                R.id.profileFragment -> binding.btmMenu.isVisible = true
                else -> binding.btmMenu.visibility = View.GONE
            }
        }
//        binding.btmMenu.setOnItemSelectedListener{
//            when(it.itemId){
//                R.id.item_main ->{
//                    loadFragment(FCoursesFragment())
//                    true
//                }
//                R.id.item_history ->{
//                    loadFragment(PCoursesFragment())
//                    true
//                }
//                R.id.item_favor ->{
//                    loadFragment(FavoriteFragment())
//                    true
//                }
//                R.id.item_profile ->{
//                    loadFragment(ProfileFragment())
//                    true
//                }
//                else -> false
//            }
//        }
    }

//    private fun loadFragment(fragment: Fragment, ){
//        val transaction = supportFragmentManager.beginTransaction()
//        transaction.replace(R.id.fragmentContainerView, fragment)
//        transaction.commit()
//    }

    companion object{
        fun start(context: Context){
            context.startActivity(Intent(context, MenuActivity::class.java))
        }
    }
}