package com.example.platformforschoolchildren.ui.p_courses

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.core.base.BaseFragment
import com.example.platformforschoolchildren.data.data
import com.example.platformforschoolchildren.databinding.FragmentPCourcesBinding
import com.example.network.models.PCourse
import com.example.platformforschoolchildren.ui.main.MainVM

class PCoursesFragment : BaseFragment<MainVM, FragmentPCourcesBinding>(),
    PCourseAdapter.ItemClickListener {

    private var adapter = PCourseAdapter(this)
    override fun getViewModelClass() = MainVM::class.java
    override fun getViewBinding() = FragmentPCourcesBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupView()
        initListener()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setupView(){
        binding.rvPCourses.layoutManager = LinearLayoutManager(context)
        binding.rvPCourses.adapter = adapter

        adapter.items = data.getPList()
        adapter.notifyDataSetChanged()
    }

    private fun initListener(){
        binding.btnHide.setOnClickListener {
            binding.rlFormIncours.isVisible = false }
    }

    override fun onItemClick(item: PCourse) {
        val action = PCoursesFragmentDirections.actionPCoursesFragmentToPCoursesDetailsFragment(item)
        findNavController().navigate(action)
    }

    override fun onEnrollClick() {
        binding.rlFormIncours.visibility = View.VISIBLE
    }

}