package com.example.platformforschoolchildren.ui.verification.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation
import com.example.core.base.BaseFragment
import com.example.network.models.LoginQuery
import com.example.platformforschoolchildren.R
import com.example.platformforschoolchildren.databinding.FragmentLoginBinding
import com.example.platformforschoolchildren.network.repository.Repository
import com.example.platformforschoolchildren.ui.main.MenuActivity
import com.example.platformforschoolchildren.ui.verification.ChooseActionFragment
import com.example.platformforschoolchildren.ui.verification.registration.RegistFragment
import com.example.platformforschoolchildren.ui.verification.repassword.RessetPassFragment

class LoginFragment : BaseFragment<LoginVM, FragmentLoginBinding>(){

    override fun getViewModelClass() = LoginVM::class.java

    override fun getViewBinding() = FragmentLoginBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        initListeners()
    }

    private fun setupView() {

    }

    private fun initListeners() {

        binding.btnSignin.setOnClickListener {
            view?.let { it1 ->
                Navigation.findNavController(it1)
                    .navigate(R.id.action_loginFragment_to_FCoursesFragment)
            }
        }

        binding.rePass.setOnClickListener {
            view?.let { it1 ->
                Navigation.findNavController(it1)
                    .navigate(R.id.action_loginFragment_to_ressetPassFragment)
            }
        }
        binding.btnSigninEmail.setOnClickListener { }
        binding.btnSignin.setOnClickListener {
            makeLogin()
            view?.let { it1 ->
                Navigation.findNavController(it1)
                    .navigate(R.id.action_loginFragment_to_FCoursesFragment)
            }
        }
    }

    fun makeLogin() {
        val param =
            LoginQuery(binding.etInputEmail.text.toString(), binding.etInputPass.text.toString())

        viewModel.makeLogin(param)
    }

    fun subLiveData(){
        viewModel.event.observe(this, Observer {

        })
    }

    companion object {
        fun newInstance() = LoginFragment()
    }
}