package com.example.platformforschoolchildren.ui.verification

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.core.base.BaseFragment
import com.example.platformforschoolchildren.R
import com.example.platformforschoolchildren.databinding.FragmentChooseActionBinding
import com.example.platformforschoolchildren.ui.verification.login.LoginFragment
import com.example.platformforschoolchildren.ui.verification.login.LoginVM
import com.example.platformforschoolchildren.ui.verification.registration.RegistFragment

class ChooseActionFragment : BaseFragment<LoginVM, FragmentChooseActionBinding>() {
    override fun getViewModelClass() = LoginVM::class.java

    override fun getViewBinding() = FragmentChooseActionBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        initListeners()
    }

    private fun setupView(){

    }

    private fun initListeners(){

        binding.btnLogin.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_chooseActionFragment_to_loginFragment) }
        }

        binding.btnRegist.setOnClickListener {
            view?.let { it1 -> Navigation.findNavController(it1).navigate(R.id.action_chooseActionFragment_to_registFragment) }
        }

    }
}