package com.example.platformforschoolchildren.ui.verification.login

import androidx.lifecycle.MutableLiveData
import com.example.core.base.CoreViewModel
import com.example.core.base.Event
import com.example.core.base.ResponseHandler
import com.example.network.api.AppApi
import com.example.network.models.LoginQuery
import com.example.network.models.LoginResponse
import com.example.network.models.RegisterQuery
import com.example.network.models.RegisterResponse
import com.example.network.models.RepassQuery
import com.example.network.models.RepassResponse
import com.example.platformforschoolchildren.network.repository.Repository
import java.util.prefs.Preferences

class LoginVM (private val repo: Repository,
               private val prefs: Preferences): CoreViewModel<Event>(){


    fun makeLogin(params: LoginQuery){
        showProgress()
        disposable.add(repo
            .makeLogin(params)
            .doFinally { hideProgress() }
            .subscribeWith(object : ResponseHandler<LoginResponse>(exceptionHandler,
                {
                    if (it.auth_token.isEmpty() && it.ref_token.isEmpty()){
                        event.value = Event.Fail("Fatal error")
                    }else event.value = Event.Success()
                }){})
        )
    }

    fun resetPassword(param: RepassQuery){
        showProgress()
        disposable.add(repo
            .resetPassword(param)
            .doFinally { hideProgress() }
            .subscribeWith(object: ResponseHandler<RepassResponse>(exceptionHandler,
                {
                    if (it.message == "OK"){
                        event.value = Event.Success()
                    }
                }){})
        )
    }

    fun registration(params: RegisterQuery){
        showProgress()
        disposable.add(repo
            .registration(params)
            .doFinally { hideProgress() }
            .subscribeWith(object: ResponseHandler<RegisterResponse>(exceptionHandler,
                {
                    if (it.id.isNotEmpty()){
                        event.value = Event.Success()
                    }
                }){})
        )
    }

}