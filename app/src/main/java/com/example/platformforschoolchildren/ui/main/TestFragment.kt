package com.example.platformforschoolchildren.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.core.base.BaseFragment
import com.example.platformforschoolchildren.R
import com.example.platformforschoolchildren.databinding.FragmentTestBinding

class TestFragment : BaseFragment<MainVM, FragmentTestBinding>() {
    override fun getViewModelClass() = MainVM::class.java

    override fun getViewBinding() = FragmentTestBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    fun setupViews(){

    }
}