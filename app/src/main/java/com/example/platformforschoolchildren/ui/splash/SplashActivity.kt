package com.example.platformforschoolchildren.ui.splash

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.platformforschoolchildren.R
import com.example.platformforschoolchildren.ui.main.MenuActivity

@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity(){
    private val SPLASH_TIME_OUT: Long = 1500

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            MenuActivity.start(this)
            finishAffinity()
        }, SPLASH_TIME_OUT)
    }

    fun checkAuth(){

    }

//    override fun getViewBinding() = ActivitySplashBinding.inflate(layoutInflater)

}