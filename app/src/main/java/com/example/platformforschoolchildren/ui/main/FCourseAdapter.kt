package com.example.platformforschoolchildren.ui.main

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.platformforschoolchildren.R
import com.example.network.models.FCourse
import com.example.platformforschoolchildren.databinding.ItemFCoursesBinding
import java.lang.IllegalStateException


class FCourseAdapter(private val clickListener: ItemClickListener): RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    var items: MutableList<FCourse> = mutableListOf()

    interface ItemClickListener{
        fun onItemClick(item: FCourse)
//        fun favAction(item: Task, index: Int)
    }

//    fun toFavorite(index: Int) {
//        items[index].isFav = false
//        notifyItemChanged(index)
//        }
//
//    fun fromFavorite(index: Int){
//        items[index].isFav = true
//        notifyItemChanged(index)
//    }

    @SuppressLint("NotifyDataSetChanged")
    fun setFilteredList(filteredList: MutableList<FCourse>){
        this.items = filteredList
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if(items.isEmpty()) 0 else 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when(viewType){
            0 -> EmptyHolder.create(parent)
            1 -> Holder.create(parent, clickListener)
            else -> throw IllegalStateException("Unknown view")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is Holder -> {
                holder.bind(items[position], position)
            }
        }
    }

    override fun getItemCount(): Int  = if (items.isEmpty()) 1 else items.size

    class EmptyHolder private constructor(view: View) : RecyclerView.ViewHolder(view){
        companion object{
            fun create(parent: ViewGroup): EmptyHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_empty, parent, false)
                return EmptyHolder(view)
            }
        }
    }

    class Holder private constructor(private val view: View): RecyclerView.ViewHolder(view){
        lateinit var binding: ItemFCoursesBinding
        lateinit var item: FCourse
        var index: Int = 0
//        var pref = Preferences(App.context())

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n", "SimpleDateFormat",
            "ResourceAsColor")
        fun bind(_item: FCourse, position: Int){
            item = _item
            index = position
            binding = ItemFCoursesBinding.bind(view)

//            val fav = ContextCompat.getDrawable(view.context, R.drawable.ic_favorite)
//            val not_fav = ContextCompat.getDrawable(view.context, R.drawable.ic_favorite_border)

            item.apply{
                binding.tvFcoursName.text = item.name
                binding.tvFcoursDuration.text = item.duraction
                binding.tvFcoursSize.text = item.size.toString()
//                view.btn_favor.background = if(item.isFav) fav else not_fav
//                var author_id = Preferences(itemView.context).authorId
//                item.isFav = item.favorites.contains(author_id!!.toInt())
//                view.btn_add_fav.background = if(item.isFav) fav else not_fav
            }
        }

        companion object{
            fun create(parent: ViewGroup, clickListener: ItemClickListener): Holder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_f_courses, parent, false)
                return Holder(view).apply {
//                    itemView.btn_add_fav.setOnClickListener{
//                        clickListener.favAction(item, index)
//                    }
                    itemView.setOnClickListener {
                        clickListener.onItemClick(item)
                    }
                }
            }
        }

    }

}