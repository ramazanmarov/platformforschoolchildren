package com.example.platformforschoolchildren.ui.p_courses

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.platformforschoolchildren.R
import com.example.network.models.PCourse
import com.example.platformforschoolchildren.databinding.ItemPCoursesBinding
import java.lang.IllegalStateException


class PCourseAdapter(private val clickListener: ItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var items: MutableList<PCourse> = mutableListOf()

    interface ItemClickListener {
        fun onItemClick(item: PCourse)
        fun onEnrollClick()
//        fun favAction(item: Task, index: Int)
    }

//    fun toFavorite(index: Int) {
//        items[index].isFav = false
//        notifyItemChanged(index)
//        }
//
//    fun fromFavorite(index: Int){
//        items[index].isFav = true
//        notifyItemChanged(index)
//    }

    @SuppressLint("NotifyDataSetChanged")
    fun setFilteredList(filteredList: MutableList<PCourse>) {
        this.items = filteredList
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (items.isEmpty()) 0 else 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            0 -> EmptyHolder.create(parent)
            1 -> Holder.create(parent, clickListener)
            else -> throw IllegalStateException("Unknown view")
        }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is Holder -> {
                holder.bind(items[position], position)
            }
        }
    }

    override fun getItemCount(): Int = if (items.isEmpty()) 1 else items.size

    class EmptyHolder private constructor(view: View) : RecyclerView.ViewHolder(view) {
        companion object {
            fun create(parent: ViewGroup): EmptyHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_empty, parent, false)
                return EmptyHolder(view)
            }
        }
    }

    class Holder private constructor(private val view: View) : RecyclerView.ViewHolder(view) {

        lateinit var item: PCourse
//        lateinit var binding: ItemPCoursesBinding

        var index: Int = 0
//        var pref = Preferences(App.context())

        @SuppressLint("UseCompatLoadingForDrawables", "SetTextI18n", "SimpleDateFormat",
            "ResourceAsColor")
        fun bind(_item: PCourse, position: Int) {
            item = _item
            index = position
//            binding = ItemPCoursesBinding.bind(view)

//            val fav = ContextCompat.getDrawable(view.context, R.drawable.ic_favorite)
//            val not_fav = ContextCompat.getDrawable(view.context, R.drawable.ic_favorite_border)

            item.apply {
//                binding.tvPcoursName.text = item.name
//                binding.tvPcoursDuration.text = item.duraction
//                binding.tvPcoursSize.text = item.size.toString()
//                binding.tvPcoursPrice.text = item.price.toString()
//                binding.tvPcoursStart.text = item.startDate
//                var author_id = Preferences(itemView.context).authorId
//                item.isFav = item.favorites.contains(author_id!!.toInt())
//                view.btn_favor.background = if (item.isFav) fav else not_fav
            }
        }

        companion object {
            fun create(parent: ViewGroup, clickListener: ItemClickListener): Holder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_p_courses, parent, false)
                return Holder(view).apply {
//                    itemView.btn_add_fav.setOnClickListener{
//                        clickListener.favAction(item, index)
//                    }
                    itemView.setOnClickListener {
                        clickListener.onItemClick(item)
                    }
//                    view.btnEnroll.setOnClickListener {
//                        clickListener.onEnrollClick()
//                    }
                }
            }
        }

    }

}