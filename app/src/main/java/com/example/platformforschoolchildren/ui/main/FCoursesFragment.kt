package com.example.platformforschoolchildren.ui.main

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.platformforschoolchildren.R
import com.example.core.base.BaseFragment
import com.example.core.storage.Preferences
import com.example.platformforschoolchildren.data.data
import com.example.platformforschoolchildren.databinding.FragmentFCourseBinding
import com.example.network.models.FCourse

class FCoursesFragment : BaseFragment<MainVM, FragmentFCourseBinding>(),
    FCourseAdapter.ItemClickListener {

    override fun getViewModelClass() = MainVM::class.java

    override fun getViewBinding() = FragmentFCourseBinding.inflate(layoutInflater)

    lateinit var pref: Preferences
    private var adapter = FCourseAdapter(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupViews()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setupViews() {

        binding.rcViewFreeCourses.layoutManager = LinearLayoutManager(context)
        binding.rcViewFreeCourses.adapter = adapter

        adapter.items = data.getFList()
        adapter.notifyDataSetChanged()

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query.isEmpty()) {
                    requireContext().hideKeyboard(requireView())
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
//                getFilterList(newText)
                return true
            }
        })
    }

    private fun initListeners(){

    }

    @SuppressLint("NotifyDataSetChanged")
//    private fun getFilterList(query: String?) {
//        val mList = adapter.items
//        if (query != null) {
//            val filteredList = ArrayList<FCourse>()
//            for (i in mList) {
//                if (i.category.name.lowercase(Locale.ROOT).contains(query) || i.price.toString()
//                        .contains(query)
//                ) {
//                    filteredList.add(i)
//                }
//            }
//            if (filteredList.isEmpty()) {
//                Toast.makeText(requireActivity(), "Нет данных подходящих под описание", Toast.LENGTH_SHORT).show()
//            } else {
//                adapter.setFilteredList(filteredList)
//                adapter.notifyDataSetChanged()
//            }
//        }
//    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onItemClick(item: FCourse) {
        val action = FCoursesFragmentDirections.actionFCoursesFragmentToFCourseDetailsFragment(item)
        findNavController().navigate(action)
    }


}