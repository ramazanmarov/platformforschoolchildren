package com.example.platformforschoolchildren.ui.p_courses

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import com.example.core.base.BaseFragment
import com.example.platformforschoolchildren.R
import com.example.platformforschoolchildren.databinding.FragmentPCoursesDetailsBinding
import com.example.platformforschoolchildren.ui.main.MainVM
import com.example.platformforschoolchildren.ui.verification.login.LoginVM
import com.example.platformforschoolchildren.ui.verification.registration.Regist2FragmentArgs

class PCoursesDetailsFragment : BaseFragment<MainVM, FragmentPCoursesDetailsBinding>() {

    val args: PCoursesDetailsFragmentArgs by navArgs()
    override fun getViewModelClass() = MainVM::class.java

    override fun getViewBinding() = FragmentPCoursesDetailsBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        initListeners()
    }

    fun setupView(){

    }

    fun initListeners(){
        binding.btnEnroll.setOnClickListener {
            binding.rlForm.isVisible = true }

        binding.btnHide.setOnClickListener {
            binding.rlForm.isVisible = false}
    }

}