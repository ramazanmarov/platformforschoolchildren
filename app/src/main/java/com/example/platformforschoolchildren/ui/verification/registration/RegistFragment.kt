package com.example.platformforschoolchildren.ui.verification.registration

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.example.core.base.BaseFragment
import com.example.platformforschoolchildren.databinding.FragmentRegistBinding
import com.example.platformforschoolchildren.ui.verification.login.LoginVM

class RegistFragment : BaseFragment<LoginVM, FragmentRegistBinding>() {
    override fun getViewModelClass() = LoginVM::class.java

    override fun getViewBinding() = FragmentRegistBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        initListeners()
    }


    private fun setupView(){

    }

    @SuppressLint("SuspiciousIndentation")
    private fun initListeners(){

        binding.btnNext.setOnClickListener {
            makeRegist()
        }

    }

    fun makeRegist(){
        if (binding.etInputEmail.text.isNullOrEmpty() && binding.etInputPass.text.isNullOrEmpty()){
            Toast.makeText(requireContext(), "Заполните поля!", Toast.LENGTH_SHORT).show()
        }else{
            val action = RegistFragmentDirections
                .actionRegistFragmentToRegist2Fragment()
                .setPass(binding.etInputPass.text.toString())
                .setEmailPass(binding.etInputEmail.text.toString())
            view?.let { it2 -> Navigation.findNavController(it2).navigate(action) }
        }
    }

}