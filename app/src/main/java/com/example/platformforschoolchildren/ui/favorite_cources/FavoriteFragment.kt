package com.example.platformforschoolchildren.ui.favorite_cources

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.core.base.BaseFragment
import com.example.network.models.FCourse
import com.example.network.models.PCourse
import com.example.platformforschoolchildren.R
import com.example.platformforschoolchildren.data.data
import com.example.platformforschoolchildren.databinding.FragmentFavoriteBinding
import com.example.platformforschoolchildren.ui.main.FCourseAdapter
import com.example.platformforschoolchildren.ui.main.MainVM
import com.example.platformforschoolchildren.ui.p_courses.PCourseAdapter
import com.example.platformforschoolchildren.ui.verification.login.LoginVM

class FavoriteFragment : BaseFragment<MainVM, FragmentFavoriteBinding>(),
    FCourseAdapter.ItemClickListener, PCourseAdapter.ItemClickListener {

    private var fCourseAdapter = FCourseAdapter(this)
    override fun getViewModelClass() = MainVM::class.java

    override fun getViewBinding() = FragmentFavoriteBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        initListeners()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setupView(){
        binding.rvCoursec.layoutManager = LinearLayoutManager(context)
        binding.rvCoursec.adapter = fCourseAdapter
        fCourseAdapter.items = data.getFList()
        fCourseAdapter.notifyDataSetChanged()

    }

    fun initListeners(){

        binding.btnHide.setOnClickListener { binding.rlForm.isVisible = false }
        binding.btnDone.setOnClickListener { binding.rlForm.isVisible = false }
        binding.btnFree.setOnClickListener { toggleButtons(true) }
        binding.btnPaid.setOnClickListener { toggleButtons(false) }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun toggleButtons(toList: Boolean){
        if (toList){
            binding.btnPaid.setBackgroundColor(Color.TRANSPARENT)
            binding.btnPaid.setTextColor(requireContext().getColor(R.color.black))

            binding.btnFree.background = requireContext().getDrawable(R.drawable.round_corner_button)
            binding.btnFree.setTextColor(requireContext().getColor(R.color.white))

            binding.rvCoursec.adapter = fCourseAdapter

            fCourseAdapter.items = data.getFList()
            fCourseAdapter.notifyDataSetChanged()



        }else {
            binding.btnFree.setBackgroundColor(Color.TRANSPARENT)
            binding.btnFree.setTextColor(requireContext().getColor(R.color.black))

            binding.btnPaid.background = requireContext().getDrawable(R.drawable.round_corner_button)
            binding.btnPaid.setTextColor(requireContext().getColor(R.color.white))

            val fCourseAdapter = PCourseAdapter(this)
            binding.rvCoursec.adapter = fCourseAdapter

            fCourseAdapter.items = data.getPList()
            fCourseAdapter.notifyDataSetChanged()


        }
    }

    override fun onItemClick(item: FCourse) {
        val action = FavoriteFragmentDirections.actionFavoriteFragmentToFCourseDetailsFragment(item)
        findNavController().navigate(action)
    }

    override fun onItemClick(item: PCourse) {
        val action = FavoriteFragmentDirections.actionFavoriteFragmentToPCoursesDetailsFragment(item)
        findNavController().navigate(action)
    }

    override fun onEnrollClick() {
        binding.rlForm.visibility = View.VISIBLE
    }
}