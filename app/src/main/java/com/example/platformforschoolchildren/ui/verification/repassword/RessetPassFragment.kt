package com.example.platformforschoolchildren.ui.verification.repassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.core.base.BaseFragment
import com.example.platformforschoolchildren.R
import com.example.platformforschoolchildren.databinding.FragmentRessetpassBinding
import com.example.platformforschoolchildren.ui.verification.login.LoginVM
import com.example.platformforschoolchildren.ui.verification.registration.RegistFragment

class RessetPassFragment : BaseFragment<LoginVM, FragmentRessetpassBinding>() {
    override fun getViewModelClass() = LoginVM::class.java

    override fun getViewBinding() = FragmentRessetpassBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        initListeners()
    }

    private fun setupView(){

    }

    private fun initListeners(){


    }
}